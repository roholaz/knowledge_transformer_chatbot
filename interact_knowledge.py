# # Copyright (c) 2019-present, HuggingFace Inc.
# All rights reserved.
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree.
import logging
import random
from argparse import ArgumentParser
from itertools import chain
from pprint import pformat
import warnings

import torch
import torch.nn.functional as F

from infer_config import InferConfig
from transformers import OpenAIGPTLMHeadModel, OpenAIGPTTokenizer, GPT2LMHeadModel, GPT2Tokenizer
from train_knowledge_transformer import SPECIAL_TOKENS, build_input_from_segments, add_special_tokens_
from utils import get_fun_fact_sources, download_pretrained_model, get_knowledge_sources
import pickle

def top_filtering(logits, top_k=0, top_p=0.0, threshold=-float('Inf'), filter_value=-float('Inf')):
    """ Filter a distribution of logits using top-k, top-p (nucleus) and/or threshold filtering
        Args:
            logits: logits distribution shape (vocabulary size)
            top_k: <=0: no filtering, >0: keep only top k tokens with highest probability.
            top_p: <=0.0: no filtering, >0.0: keep only a subset S of candidates, where S is the smallest subset
                whose total probability mass is greater than or equal to the threshold top_p.
                In practice, we select the highest probability tokens whose cumulative probability mass exceeds
                the threshold top_p.
            threshold: a minimal threshold to keep logits
    """
    assert logits.dim() == 1  # Only work for batch size 1 for now - could update but it would obfuscate a bit the code
    top_k = min(top_k, logits.size(-1))
    if top_k > 0:
        # Remove all tokens with a probability less than the last token in the top-k tokens
        indices_to_remove = logits < torch.topk(logits, top_k)[0][..., -1, None]
        logits[indices_to_remove] = filter_value

    if top_p > 0.0:
        # Compute cumulative probabilities of sorted tokens
        sorted_logits, sorted_indices = torch.sort(logits, descending=True)
        cumulative_probabilities = torch.cumsum(F.softmax(sorted_logits, dim=-1), dim=-1)

        # Remove tokens with cumulative probability above the threshold
        sorted_indices_to_remove = cumulative_probabilities > top_p
        # Shift the indices to the right to keep also the first token above the threshold
        sorted_indices_to_remove[..., 1:] = sorted_indices_to_remove[..., :-1].clone()
        sorted_indices_to_remove[..., 0] = 0

        # Back to unsorted indices and set them to -infinity
        indices_to_remove = sorted_indices[sorted_indices_to_remove]
        logits[indices_to_remove] = filter_value

    indices_to_remove = logits < threshold
    logits[indices_to_remove] = filter_value

    return logits


def sample_sequence2(model, length, context, num_samples=1, temperature=1, top_k=0, top_p=0.0, repetition_penalty=1.0, device='cpu'):
    context = torch.tensor(context, dtype=torch.long, device=device)
    context = context.unsqueeze(0).repeat(num_samples, 1)
    generated = context
    with torch.no_grad():
        for _ in range(length):

            instance = build_input_from_segments(knowledge, fun_facts, history, current_output, tokenizer, with_eos=False)

            outputs = model(**inputs)
            next_token_logits = outputs[0][:, -1, :] / (temperature if temperature > 0 else 1.)

            # repetition penalty from CTRL (https://arxiv.org/abs/1909.05858)
            for i in range(num_samples):
                for _ in set(generated[i].tolist()):
                    next_token_logits[i, _] /= repetition_penalty

            #filtered_logits = top_k_top_p_filtering(next_token_logits, top_k=top_k, top_p=top_p)
            filtered_logits = top_filtering(next_token_logits, top_k=top_k, top_p=top_p)
            if temperature == 0:  # greedy sampling:
                next_token = torch.argmax(filtered_logits, dim=-1).unsqueeze(-1)
            else:
                next_token = torch.multinomial(F.softmax(filtered_logits, dim=-1), num_samples=1)
            generated = torch.cat((generated, next_token), dim=1)
    return generated


def sample_sequence(knowledge, fun_facts, history, tokenizer, model, config, current_output=None):
    special_tokens_ids = tokenizer.convert_tokens_to_ids(SPECIAL_TOKENS)
    if current_output is None:
        current_output = []

    num_samples = 1
    knowledge = list(chain(*knowledge))
    fun_facts = list(chain(*fun_facts))
    repetition_penalty = 1.2
    #history = list(chain(*history))

    for i in range(config.max_length):
        instance = build_input_from_segments(knowledge, fun_facts, history, current_output, tokenizer, with_eos=False)

        input_ids = torch.tensor(instance["input_ids"], device=config.device).unsqueeze(0)
        token_type_ids = torch.tensor(instance["token_type_ids"], device=config.device).unsqueeze(0)

        logits = model(input_ids, token_type_ids=token_type_ids)
        if isinstance(logits, tuple):  # for gpt2 and maybe others
            logits = logits[0]
        logits = logits[0, -1, :] / config.temperature

        for i in range(num_samples):
            for _ in set(input_ids[i].tolist()):
                logits[ _] /= repetition_penalty

        logits = top_filtering(logits, top_k=config.top_k, top_p=config.top_p)
        probs = F.softmax(logits, dim=-1)

        prev = torch.topk(probs, 1)[1] if config.no_sample else torch.multinomial(probs, 1)
        if i < config.min_length and prev.item() in special_tokens_ids:
            while prev.item() in special_tokens_ids:
                if probs.max().item() == 1:
                    warnings.warn("Warning: model generating special token with probability 1.")
                    break  # avoid infinitely looping over special token
                prev = torch.multinomial(probs, num_samples=1)

        if prev.item() in special_tokens_ids:
            break
        current_output.append(prev.item())

    return current_output


def run():
    config_file = "configs/infer_all_data_bert.json"
    config = InferConfig.from_json_file(config_file)

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__file__)
    logger.info(pformat(config))

    if config.model_checkpoint == "":
        config.model_checkpoint = download_pretrained_model()

    random.seed(config.seed)
    torch.random.manual_seed(config.seed)
    torch.cuda.manual_seed(config.seed)

    logger.info("Get pretrained model and tokenizer")
    tokenizer_class = GPT2Tokenizer if "gpt2" == config.model else OpenAIGPTTokenizer
    tokenizer = tokenizer_class.from_pretrained(config.model_checkpoint, cache_dir=config.cache_dir)
    model_class = GPT2LMHeadModel if "gpt2" == config.model else OpenAIGPTLMHeadModel
    model = model_class.from_pretrained(config.model_checkpoint, cache_dir=config.cache_dir)
    model.to(config.device)
    add_special_tokens_(model, tokenizer)

    logger.info("Sample a selected_knowledge")
    #personalities = get_dataset_personalities(tokenizer, config.dataset_path, config.dataset_cache)
    #all_knowledges = get_knowledge_sources(tokenizer, config.dataset_path, config.dataset_cache)
    #all_fun_facts = get_fun_fact_sources(tokenizer, config.dataset_path, config.dataset_cache)
    #pickle.dump(all_knowledges, open("knowledges.p", "wb"))
    all_fun_facts = pickle.load(open("caches/fun_facts.p", "rb"))
    all_knowledges = pickle.load(open("caches/knowledges.p", "rb"))
    all_knowledges = [k for k in all_knowledges if k != []]
    all_fun_facts = [f for f in all_fun_facts if f !=[]]
    num_train = len(all_fun_facts)
    i = random.randint(0, num_train)
    selected_knowledge = [all_knowledges[i][0]]
    selected_fun_facts = [all_fun_facts[i][0]]

    logger.info("Selected selected_knowledge: %s", tokenizer.decode(chain(*selected_knowledge)))
    logger.info("Selected fun_facts: %s", tokenizer.decode(chain(*selected_fun_facts)))

    history = []
    while True:
        raw_text = input(">>> ")
        while not raw_text:
            print('Prompt should not be empty!')
            raw_text = input(">>> ")
        history.append(tokenizer.encode(raw_text))
        with torch.no_grad():
            out_ids = sample_sequence(selected_knowledge, selected_fun_facts, history, tokenizer, model, config)
            # out_ids = sample_sequence2(model, config.max_length, context, num_samples=1,
            #                            temperature=config.temperature,
            #                            top_k=config.top_k,
            #                            top_p=config.top_p,
            #                            repetition_penalty=1.0)
        history.append(out_ids)
        history = history[-(2 * config.max_history + 1):]
        out_text = tokenizer.decode(out_ids, skip_special_tokens=True)
        print(out_text)


if __name__ == "__main__":
    run()
