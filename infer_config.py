import json


class InferConfig(object):

    def __init__(self,
                 dataset_path="",
                 dataset_cache="",
                 model_checkpoint="",
                 cache_dir="",
                 max_history="",
                 device="",
                 no_sample=True,
                 max_length=20,
                 min_length=1,
                 seed=42,
                 temperature=0.7,
                 top_k=0,
                 top_p=0.9
                 ):
        self.dataset_path = dataset_path
        self.dataset_cache = dataset_cache
        self.model_checkpoint = model_checkpoint
        self.cache_dir = cache_dir
        self.max_history = max_history
        self.device = device
        self.no_sample = no_sample
        self.max_length = max_length
        self.min_length = min_length
        self.seed = seed
        self.temperature = temperature
        self.top_k = top_k
        self.top_p = top_p

    @classmethod
    def from_dict(cls, json_object):
        config = InferConfig()
        for key in json_object:
            config.__dict__[key] = json_object[key]
        return config

    @classmethod
    def from_json_file(cls, json_file):
        with open(json_file) as f:
            config_json = f.read()

        return cls.from_dict(json.loads(config_json))
